import React from 'react'
import { View, Image, StyleSheet, TouchableWithoutFeedback, Text } from 'react-native'
import { w } from '../../../constans'
import { hidden } from 'ansi-colors';


const ImageCard = ({ data }) => {

    const { container, cover, coverText,userName } = styles

    onPressButton = () => {
        alert('Was clicked')
    }


    return (
        <View style={container}>
            <TouchableWithoutFeedback onPress={this.onPressButton}>
                <Image style={cover} source={{ uri: data.urls.small }} />
            </TouchableWithoutFeedback>
            <View style={coverText}>
                <Text style={userName}>{data.user.name}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: w / 2.2,
        paddingVertical: 10
    }, 

    cover: {
        width: w / 2.2,
        height: w * 0.83,
        borderRadius: 10
    },

    coverText: {
        position: 'absolute',
        bottom: 10,
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        borderBottomEndRadius: 10,
        borderBottomStartRadius: 10,
    },

    userName: {
        color: '#fff',
        textAlign: 'center',
         display: 'none'
    }
    
})

export { ImageCard }