import React from 'react'
import { View, StyleSheet} from 'react-native'

const Layout = props => {
  const { container } = styles

  return (
      <View style={container}>
        {props.children}
      </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
    flexShrink: 2,
    justifyContent: 'space-around',
    paddingBottom: 130
  }
})


export { Layout }