import React from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'

const Header = (props) => {

    const { container, headerTitle } = styles 

    return (
        <View style={container}>
            <Text style={headerTitle}>Splash</Text>
            <TextInput placeholder="Find a picture..." />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#4287f5',
        height: 100,
        paddingTop: 50,
        position: 'relative'
    },

    headerTitle: {
        fontFamily: 'monospace',
        color: '#fff',
        fontSize: 25
    },
})

export { Header }