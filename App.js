import React, { Component } from 'react'
import { View, ActivityIndicator, ScrollView } from 'react-native'
import { Header, ImageCard, Layout } from './src/Componets/ui'




export default class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: [],
      page: 1,
      loading: true
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    const url = `https://api.unsplash.com/photos/?page=${this.state.page}&client_id=431379f67d9cf2804939e3a5213e52c3656d66d41ddf879f18950d56286a0719`
    try {
      const response = await fetch(url, {
        method: 'GET'
      })
      const dataRes = await response.json()
      this.setState({ data: this.state.data.concat(dataRes), loading: false })
    } catch (e) {
      throw e 
    }
  }

  loadNextPage = () => {
    this.setState(
      {page: this.state.page + 1},
      this.getData 
    )
  }

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
   return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1
  }


  render() {
    const { data } = this.state
    return (
      <View>
        <Header />
        <ScrollView
          onScroll={({ nativeEvent }) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.loadNextPage()
              console.warn('Reached the bottom')
            }
          }}
        > 
          <Layout>
            {data.map((img, index) => (
               <ImageCard data={img} key={index} />
            ))
            }
          </Layout>
        </ScrollView>
      </View>
    )
  }
}




