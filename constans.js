import { Dimensions } from 'react-native'

const screenSize = Dimensions.get('window')
export const h = screenSize.height
export const w = screenSize.width

